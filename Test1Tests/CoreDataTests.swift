//
//  CoreDataTests.swift
//  Test1Tests
//
//  Created by vaskov on 17.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import CoreData
@testable import Test1


class CoreDataTests: XCTestCase {
    private var disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        
        let promise = expectation(description: "It's OK")
    
        CORE_DATA_MANAGER.copyDBToDocuments().do(onNext: { (_) in
            promise.fulfill()
        }).subscribe().disposed(by: disposeBag)
        
        wait(for: [promise], timeout: 1)
    }

    override func tearDown() {
//        let promise = expectation(description: "It's OK")
//
//        CORE_DATA_MANAGER.clear().map { (_) in
//           // promise.fulfill()
//        }.subscribe().disposed(by: disposeBag)
//
//        wait(for: [promise], timeout: 1)
        
        super.tearDown()
    }

    func testContexts() {
        XCTAssert(CORE_DATA_MANAGER.mainContext != CORE_DATA_MANAGER.backgroundContext)
    }
    
    func testNew() {
        let (user1, _) = CORE_DATA_MANAGER.new(contextType: .main, responseType: DBUser.self)
        let (user2, _) = CORE_DATA_MANAGER.new(contextType: .background, responseType: DBUser.self)
        let user3: DBUser? = CORE_DATA_MANAGER.new(with: CORE_DATA_MANAGER.mainContext)
        XCTAssert(user1 != nil)
        XCTAssert(user2 != nil)
        XCTAssert(user3 != nil)
    }
    
    func testNewAndSave() {
        let promise1 = expectation(description: "It's OK1")
        let promise2 = expectation(description: "It's OK2")
        let promise3 = expectation(description: "It's OK3")
        
        CORE_DATA_MANAGER.newAndSave(contextType: .main, responseType: DBUser.self).flatMap { (user, context) ->          Observable<(DBUser?, NSManagedObjectContext)> in
            user?.friend = CORE_DATA_MANAGER.new(with: context)
            user?.friend?.id = "5555"
            
            if user != nil { promise1.fulfill() }
            return CORE_DATA_MANAGER.newAndSave(contextType: .background, responseType: DBUser.self)
        }.flatMap { (user, context) -> Observable<[DBUser]> in
            if user != nil { promise2.fulfill() }
            user?.friend = CORE_DATA_MANAGER.new(with: context)
            user?.friend?.id = "55551"

            let predicate = NSPredicate(format: "friend.id CONTAINS[cd] %@", "5555")
            return CORE_DATA_MANAGER.read(with: context, predicate: predicate, responseType: DBUser.self)
        }.map({ users in
            if users.count > 0 { promise3.fulfill() }
        }).subscribe().disposed(by: disposeBag)
        
        wait(for: [promise1, promise2, promise3], timeout: 1)
    }
    
    func testSaveAndRead() {
        let promise1 = expectation(description: "It's OK1")
        let promise2 = expectation(description: "It's OK2")
        let promise3 = expectation(description: "It's OK3")
        let promise4 = expectation(description: "It's OK4")
        
        CORE_DATA_MANAGER.save(contextType: .main).flatMap { (context) -> Observable<NSManagedObjectContext> in
            let user: DBUser? = CORE_DATA_MANAGER.new(with: context)
            user?.friend = CORE_DATA_MANAGER.new(with: context)
            user?.friend?.id = "6666"
            return CORE_DATA_MANAGER.save(contextType: .background)
        }.flatMap { (context) -> Observable<([DBUser])> in
            let user: DBUser? = CORE_DATA_MANAGER.new(with: context)
            user?.friend = CORE_DATA_MANAGER.new(with: context)
            user?.friend?.id = "66661"
            
            let predicate = NSPredicate(format: "friend.id CONTAINS[cd] %@", "6666")
            return CORE_DATA_MANAGER.read(with: context, predicate: predicate, responseType: DBUser.self)
        }.flatMap { users -> Observable<(([DBUser], NSManagedObjectContext), [DBUser])> in
            if users.count > 0 { promise3.fulfill() }
            
            let predicate = NSPredicate(format: "friend.id CONTAINS[cd] %@", "6666")
            let obs1 = CORE_DATA_MANAGER.read(predicate: predicate, responseType:  DBUser.self)
            let obs2 = Observable.just(users)
            return Observable.combineLatest(obs1, obs2)
        }.map({ (arg0, users1) in
            let (users2, context) = arg0
            if users2.count > 0 { promise3.fulfill() }
        }).subscribe().disposed(by: disposeBag)
        
        wait(for: [promise1, promise2, promise3], timeout: 1)
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
