//
//  AddViewController.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private(set) var viewModel: AddViewModel!
    
    lazy var tableManager: FriendTableManager = {
        let manager = FriendTableManager()
        manager.configManager(tableView, isEdit: false, delegate: self)
        return manager
    }()
    
    override func configureRX() {
        viewModel = AddViewModel()
        
        viewModel.error.asObservable().map { [weak self] (error) in
            Test1Error.showAlert(with: error)
            self?.hideHUD()
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.friends.asObservable().map { [weak self] (friends) in
            self?.tableManager.items = friends
            self?.tableView.isHidden = friends.count == 0
            friends.count == 0 ? self?.showHUD() : self?.hideHUD()
            self?.tableView.reloadData()
        }.subscribe().disposed(by: disposeBag)
        
        viewModel.start()
    }
}

extension AddViewController: FriendTableManagerDelegate {
    func manager(_ manager: FriendTableManager, didRemove friend: Friend) {      
    }
    
    func manager(_ manager: FriendTableManager, didSelectAt indexPath: IndexPath) {
        let friend = manager.items[indexPath.row]
        let predicate = NSPredicate(format: "id = %@", friend.id!)
        
        CORE_DATA_MANAGER.update(contextType: .background, predicate: predicate, responseType: DBFriend.self).map { [weak self] (arg) in
            let (friends, context) = arg
            if friends.count == 0 {
                _ = DBUser.new(with: friend, context: context)
            }
            DispatchQueue.main.async {
                manager.items.remove(at: indexPath.row)
                self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }.catchError { (error) in
            Test1Error.showAlert(with: error)
            return Observable<Void>.just(())
        }.subscribe().disposed(by: disposeBag)
    }
    
    func manager(_ manager: FriendTableManager, model: Friend) {
    }
    
    func manager(_ manager: FriendTableManager, willDisplay model: Friend) {
        if let response = viewModel.response, response.hasMoreItems, model.id == Constants.emptyId {
            response.hasMoreItems = false
            viewModel.next()
        }
    }
}
