//
//  AddViewModel.swift
//  Test1
//
//  Created by vaskov on 2/18/19.
//  Copyright (c) 2019 Home. All rights reserved.
//
//

import Foundation
import RxSwift
import RxCocoa


final class AddViewModel  {
    var error: BehaviorRelay<NSError?> = BehaviorRelay(value: nil)
    var friends: BehaviorRelay<[Friend]> = BehaviorRelay(value: [])
    var response: PaginatedResponse<FriendRequest, Friend>!
    
    private let disposeBag = DisposeBag()
    
    init() {
        response = Friend.friends()
        response.delegate = self
    }
    
    func start() {
        response.startReload()
    }
    
    func next() {
        response.startNextPage()
    }
}

// MARK: - PaginatedResponseDelegate -
extension AddViewModel: PaginatedResponseDelegate {
    func loadBegan(_ response: AnyObject) {
    }
    
    func loadFinished(_ response: AnyObject) {
        let items = self.response.items.filter { (friend) -> Bool in
            return friend.id != nil && friend.id!.count > 0
        }
        friends.accept(items)
    }
    
    func loadErrored(_ response: AnyObject, error: NSError) {
        self.error.accept(error)
    }
}
