//
//  DetailViewModel.swift
//  Test1
//
//  Created by vaskov on 14.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum DefaultText: String {
    case firstName = "First name"
    case lastName = "Last name"
    case email = "Email"
    case phone = "Phone"
    
    func validate(_ isValid: Bool) -> (String, UIColor) {
        return (isValid ? self.rawValue : self.rawValue + " don't valid", isValid ? UIColor.black : UIColor.red)
    }
    
    func behaviorRelay() -> BehaviorRelay<(String, UIColor)> {
        return BehaviorRelay(value: (self.rawValue, UIColor.black))
    }
}

class DetailViewModel {
    weak var friend: Friend!
    var isDoneEnabled: Driver<Bool>!
    var validatedFirstName: BehaviorRelay<(String, UIColor)> = DefaultText.firstName.behaviorRelay()
    var validatedLastName: BehaviorRelay<(String, UIColor)> = DefaultText.lastName.behaviorRelay()
    var validatedEmail: BehaviorRelay<(String, UIColor)> = DefaultText.email.behaviorRelay()
    var validatedPhone: BehaviorRelay<(String, UIColor)> = DefaultText.phone.behaviorRelay()
    
    init(firstName: ControlProperty<String?>,
         lastName: ControlProperty<String?>,
         email: ControlProperty<String?>,
         phone: ControlProperty<String?>,
         friend: Friend) {
        self.friend = friend
        
        let isFirstNameValid = firstName.asDriver().map { [weak self] (text) -> Bool in
            let isValid = text?.isNameValid() ?? false
            self?.validatedFirstName.accept(DefaultText.firstName.validate(isValid))
            return isValid
        }
        
        let isLastNameValid = lastName.asDriver().map { [weak self] (text) -> Bool in
            let isValid = text?.isNameValid() ?? false
            self?.validatedLastName.accept(DefaultText.lastName.validate(isValid))
            return isValid
        }
        
        let isEmailValid = email.asDriver().map { [weak self] (text) -> Bool in
            let isValid = text?.isEmailValid() ?? false
            self?.validatedEmail.accept(DefaultText.email.validate(isValid))
            return isValid
        }
        
        let isPhoneValid = phone.asDriver().map { [weak self] (text) -> Bool in
            let isValid = text?.isPhoneValid() ?? false
            self?.validatedPhone.accept(DefaultText.phone.validate(isValid))
            return isValid
        }
        
        let isFirstNameChanged = firstName.asDriver().map { (text) -> Bool in
            return text != friend.name?.first
        }
        
        let isLastNameChanged = lastName.asDriver().map { (text) -> Bool in
            return text != friend.name?.last
        }
        
        let isEmailChanged = email.asDriver().map { (text) -> Bool in
            return text != friend.email
        }
        
        let isPhoneChanged = phone.asDriver().map { (text) -> Bool in
            return text != friend.phone
        }
        
        isDoneEnabled = Driver.combineLatest(isFirstNameValid, isLastNameValid, isEmailValid, isPhoneValid, isFirstNameChanged, isLastNameChanged, isEmailChanged, isPhoneChanged)
            .map({ $0 && $1 && $2 && $3 && ($4 || $5 || $6 || $7) })
            .distinctUntilChanged()
    }
}
