//
//  DetailViewController.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class DetailViewController: BaseTextFieldsViewController, CashImagesProtocol {
    @IBOutlet weak var imageView: ActivityImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var friend: Friend!
    private(set) var viewModel: DetailViewModel!
        
    override func configureLocale() {
        firstNameTextField.text = friend.name?.first ?? ""
        lastNameTextField.text = friend.name?.last ?? ""
        emailTextField.text = friend.email ?? ""
        phoneTextField.text = friend.phone ?? ""
        setImage(url: friend.picture?.large, imageView: imageView)
    }
    
    override func configureRX() {
        viewModel = DetailViewModel(firstName: firstNameTextField.rx.text,
                                    lastName: lastNameTextField.rx.text,
                                    email: emailTextField.rx.text,
                                    phone: phoneTextField.rx.text,
                                    friend: friend)
        
        viewModel.isDoneEnabled.drive(navigationItem.rightBarButtonItem!.rx.isEnabled).disposed(by: disposeBag)
        viewModel.validatedFirstName.asDriver().drive(firstNameLabel.validate).disposed(by: disposeBag)
        viewModel.validatedLastName.asDriver().drive(lastNameLabel.validate).disposed(by: disposeBag)
        viewModel.validatedEmail.asDriver().drive(emailLabel.validate).disposed(by: disposeBag)
        viewModel.validatedPhone.asDriver().drive(phoneLabel.validate).disposed(by: disposeBag)
    }
}

// MARK: - Actions -
extension DetailViewController {
    @IBAction func cancel() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done() {
        savedData()
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Privates -
extension DetailViewController {
    private func savedData() {
        let predicate = NSPredicate(format: "id=%@", friend.id!)
        
        CORE_DATA_MANAGER.update(predicate: predicate, responseType: DBFriend.self).map { [weak self] (friends, _) in
            guard let dbfriend = friends.first else {
                return
            }
            dbfriend.name?.first = self?.firstNameTextField.text
            dbfriend.name?.last = self?.lastNameTextField.text
            dbfriend.email = self?.emailTextField.text
            dbfriend.phone = self?.phoneTextField.text
        }.catchError { (error) -> Observable<()> in
            Test1Error.showAlert(with: error)
            return Observable.just(())
        }.subscribe().disposed(by: disposeBag)
    }
}

// MARK: - UILabel privates -
extension UILabel {
    fileprivate var validate: AnyObserver<(String, UIColor)> {
        return Binder(self) { label, args in
            label.text = args.0
            label.textColor = args.1
        }.asObserver()
    }
}
