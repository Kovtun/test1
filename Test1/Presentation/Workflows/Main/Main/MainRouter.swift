//
//  MainRouter.swift
//  Test1
//
//  Created by vaskov on 2/18/19.
//  Copyright (c) 2019 Home. All rights reserved.
//
//

import UIKit


final class MainRouter {
    weak var controller: BaseViewController?
    
    required init(controller: BaseViewController) {
        self.controller = controller
    }
    
    func showDetail(with friend: Friend) {
        let vc: DetailViewController = Storyboards.main.viewController()
        vc.friend = friend
        controller?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAddFriends() {
        let vc: AddViewController = Storyboards.main.viewController()
        controller?.navigationController?.pushViewController(vc, animated: true) 
    }
}
