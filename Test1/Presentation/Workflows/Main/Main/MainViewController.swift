//
//  MainViewController.swift
//  Test1
//
//  Created by vaskov on 2/18/19.
//  Copyright (c) 2019 Home. All rights reserved.
//
//

import UIKit
import RxSwift
import RxCocoa
import CoreData


final class MainViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var label: UILabel!
    
    private(set) var viewModel: MainViewModel!
    private(set) var router: MainRouter!
    
    lazy var tableManager: FriendTableManager = {
        let manager = FriendTableManager()
        manager.configManager(tableView, isEdit: true, delegate: self)
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        router = MainRouter(controller: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //#if TEST
        //viewModel.restart()
        //let user1: DBUser = CORE_DATA_MANAGER.new()!
        //#endif
    }
    
    override func configureRX() {
        viewModel = MainViewModel()
        
        viewModel.friends.asObservable().map { [weak self] (friends) in
            self?.tableManager.items = friends
            self?.label.isHidden = friends.count != 0
            self?.tableView.isHidden = friends.count == 0
            self?.tableView.reloadData()
        }.subscribe().disposed(by: disposeBag)
    }
}

// MARK: - Actions -
extension MainViewController {
    @IBAction func add() {
        router.showAddFriends()
    }
}

// MARK: - FriendTableManagerDelegate -
extension MainViewController: FriendTableManagerDelegate {
    func manager(_ manager: FriendTableManager, didRemove friend: Friend) {
        if let id = friend.id {
            let predicate = NSPredicate(format: "friend.id = %@", id)
            CORE_DATA_MANAGER.update(predicate: predicate, responseType: DBUser.self).map { (users, _) in
                if let user = users.first {
                    user.isFriend = false
                }
                self.label.isHidden = manager.items.count != 0
                self.tableView.isHidden = manager.items.count == 0
            }.catchError({ (error) -> Observable<()> in
                Test1Error.showAlert(with: error)
                return Observable.just(())
            }).subscribe().disposed(by: disposeBag)
        }
    }
    
    func manager(_ manager: FriendTableManager, didSelectAt indexPath: IndexPath) {
        let friend = manager.items[indexPath.row]
        router.showDetail(with: friend)
    }
    
    func manager(_ manager: FriendTableManager, model: Friend) { 
    }
    
    func manager(_ manager: FriendTableManager, willDisplay model: Friend) {
    }
}
