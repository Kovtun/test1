//
//  MainViewModel.swift
//  Test1
//
//  Created by vaskov on 2/18/19.
//  Copyright (c) 2019 Home. All rights reserved.
//
//

import Foundation
import RxSwift
import RxCocoa


final class MainViewModel  {
    var friends: BehaviorRelay<[Friend]> = BehaviorRelay(value: [])
    
    private let disposeBag = DisposeBag()
    
    func restart() {
        CORE_DATA_MANAGER.read(responseType: DBUser.self).map { [weak self] (users, _) in
            let friends = users.filter({ (dbuser) -> Bool in
                return dbuser.isFriend
            }).compactMap ({ (user) -> Friend? in
                return user.friend?.friend()
            })
            self?.friends.accept(friends)
        }.subscribe().disposed(by: disposeBag)
    }
}
