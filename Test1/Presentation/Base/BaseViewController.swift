//
//  BaseViewController.swift
//  Test1
//
//  Created by vaskov on 10/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD


protocol BaseViewControllerProtocol {
    func configureRX()
    func configureLocale()
}


class BaseViewController: UIViewController, BaseViewControllerProtocol {
    // MARK: - Properties -
    
    var disposeBag = DisposeBag()
    private(set) var isShowHudNow = false
    
    // MARK: - UIViewController -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLocale()
        configureRX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    deinit {
    }
    
    //MARK: This functions you can override in inheritor classes
    @objc func configureRX() {}
    func configureLocale() {}
}

// MARK: - Actions -
extension BaseViewController {
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Publics -
extension BaseViewController {
    @objc func showHUD() {
        guard !self.isShowHudNow else {
            return
        }
        self.isShowHudNow = true
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: UIApplication.shared.windows.first!, animated: true)
        }
    }
    
    @objc func hideHUD() {
        guard self.isShowHudNow else {
            return
        }
        self.isShowHudNow = false
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: UIApplication.shared.windows.first!, animated: true)
        }
    }
}


