//
//  BaseTextFieldsViewController.swift
//  Test1
//
//  Created by vaskov on 21.07.17.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit


class BaseTextFieldsViewController: BaseViewController {
    // MARK: - Properties -
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var referenceView: UIView?

    private(set) var keyboardHeight: CGFloat = 0
    var defaultBottomConstraint: CGFloat = 0.0
    var isShowKeyboard: Bool = false {
        willSet {
            guard isShowKeyboard != newValue else {
                return
            }
            willChangeShowKeyboard()
        }
    }

    // MARK: - BaseViewController -
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeNotifications()
    }
    
    // MARK: This functions you can override in inheritor classes
    func tapDoneButton() {
    }
    
    func willChangeShowKeyboard() {
    }
}

// MARK: - Actions
extension BaseTextFieldsViewController {
    @IBAction func tapOnView(_ sender: Any) {
        self.view.endEditing(true)
    }
}

// MARK: - keyboard notifications
extension BaseTextFieldsViewController {
    @objc func keyboardWillShow(notification: Notification) {
        guard !isShowKeyboard else {
            return
        }
        
        let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let rawFrame = value.cgRectValue
        let keyboardFrame = view.convert(rawFrame, from: nil)
        keyboardHeight = keyboardFrame.size.height
        bottomConstraint?.constant = keyboardHeight + defaultBottomConstraint
        self.view.layoutIfNeeded()
        scrollReferenceViewToVisibleRect(force: false)
        isShowKeyboard = true
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        guard isShowKeyboard else {
            return
        }
        bottomConstraint?.constant = CGFloat(defaultBottomConstraint)
        self.view.setNeedsLayout()
        isShowKeyboard = false
    }
}

// MARK: - UITextFieldDelegate
extension BaseTextFieldsViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        referenceView = textField
        scrollReferenceViewToVisibleRect(force: false)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            findNextTextField(after: textField.tag)
            return false
        } else {
            textField.resignFirstResponder()
        }
        if textField.returnKeyType == .done {
            tapDoneButton()
        }
        return true
    }
}

// MARK: - Privates
extension BaseTextFieldsViewController {
    fileprivate func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func removeNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func findNextTextField(after tag: Int) {
        let allTextFields = searchAllTextFieldsAndViews(inView: view)
        for textField in allTextFields {
            if textField.tag == tag + 1 {
                textField.becomeFirstResponder()
                break
            }
        }
    }
    
    fileprivate func searchAllTextFieldsAndViews(inView: UIView) -> [UIView] {
        var allTextFields = [UIView]()
        for subview in inView.subviews {
            if ((subview as? UITextField) != nil) || ((subview as? UITextView) != nil) {
                allTextFields.append(subview)
            } else  {
                let arr = searchAllTextFieldsAndViews(inView: subview)
                allTextFields.append(contentsOf: arr)
            }
        }
        return allTextFields
    }
    
    fileprivate func scrollReferenceViewToVisibleRect(force: Bool) {
        func scroll() {
            if self.referenceView != nil {
                let frame = self.referenceView!.frame
                var rect = self.referenceView!.convert(frame, to: self.scrollView); rect.origin.y += rect.height + 10
                scrollView?.scrollRectToVisible(rect, animated: true)
            }
        }
        
        if force {
            scroll()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { scroll() }
        }
    }
}
