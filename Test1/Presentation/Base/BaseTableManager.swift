//
//  TableManager.swift
//  Test1
//
//  Created by vaskov on 21.06.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


//lazy var tableManager: TTTableManager = {
//    let manager = TTTableManager()
//    manager.configManager(tableView, delegate: self)
//    return manager
//}()


class BaseTableManager<Cell: BaseTableCellProtocol, Model: Any>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell.Model == Model {
    // MARK: - Properties -
    weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    
    var items: [Model] = []
    
    var isShowRefreshControl = false {
        didSet {
            if isShowRefreshControl {
                self.configRefreshControl()
            } else {
                refreshControl.removeFromSuperview()
            }
        }
    }
    
    func configManager(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.register(UINib(nibName: Cell.identifier, bundle: nil), forCellReuseIdentifier: Cell.identifier)
    }
    
    // MARK: - UITableViewDataSource -
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = items[indexPath.row] as Model
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier) as! Cell
        cell.update(model)
        fillCell(cell, model: model, indexPath: indexPath)
        
        return cell as! UITableViewCell
    }    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
         if editingStyle == .delete {
            self.items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
         }
    }
        
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - UITableViewDelegate -
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = items[indexPath.row]
        return Cell.height(model)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func fillCell(_ cell: Cell, model: Model, indexPath: IndexPath) {}
    
    // MARK: - UIScrollViewDelegate -
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        refreshControl.alpha = scrollView.contentOffset.y / -refreshControl.height
        tableView.invalidateIntrinsicContentSize()
    }
}

// MARK: - Privates
extension BaseTableManager {
    fileprivate func configRefreshControl() {
        refreshControl.addTarget(tableView, action: #selector(UITableView.reloadData), for: .valueChanged)
        tableView.insertSubview(refreshControl, at: 0)
    }
}
