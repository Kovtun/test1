//
//  ActivityImageView.swift
//  Test1
//
//  Created by vaskov on 17.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import RxSwift

class ActivityImageView: UIImageView {
    var indicatorView = UIActivityIndicatorView.init(style: .large)
    
    private var imageObs: Observable<UIImage??>!
    private var disposeBag = DisposeBag()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.addSubview(indicatorView)
        
        indicatorView.origin = CGPoint.init(x: (self.width - indicatorView.width) / 2,
                                            y: (self.height - indicatorView.height) / 2)
    
        imageObs = self.rx.observe(Optional<UIImage>.self, "image")
        imageObs.map { [weak self] (image) in
            if self != nil {
                self!.indicatorView.isHidden = image != nil
                image == nil ? self!.indicatorView.startAnimating() :
                               self!.indicatorView.stopAnimating()
            }
        }.subscribe().disposed(by: disposeBag)
    }
}
