//
//  IntrinsicTableView.swift
//  Test1
//
//  Created by vaskov on 18/03/2019.
//  Copyright © 2019 Home. All rights reserved.
//

import UIKit


class IntrinsicTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: UIView.noIntrinsicMetric, height: self.contentSize.height)
        }
    }
    
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
}
