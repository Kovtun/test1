//  
//  FriendTableCell.swift
//  Test1
//
//  Created by vaskov on 12.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MBProgressHUD

protocol FriendTableCellDelegate: class {
}


class FriendTableCell: UITableViewCell, BaseTableCellProtocol, CashImagesProtocol {
    typealias Model = Friend
    static let identifier: String = "FriendTableCell"
    
    @IBOutlet weak var iconImageView: ActivityImageView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet var mainActivityIndicator: UIActivityIndicatorView?
    
    var model: Friend?
    weak var delegate: FriendTableCellDelegate?
    
    private var urlStr: String!    
    
    class func height(_ model: Friend) -> CGFloat {
        return 82
    }
    
    func update(_ model: Friend) {
        self.model = model
        model.id != Constants.emptyId ? mainActivityIndicator?.stopAnimating() : mainActivityIndicator?.startAnimating()
        mainActivityIndicator?.isHidden = model.id != Constants.emptyId
        iconImageView?.isHidden = model.id == Constants.emptyId
        self.isUserInteractionEnabled = model.id != Constants.emptyId
        
        if let url = model.picture?.thumbnail, urlStr != url {
            iconImageView?.image = nil
            urlStr = url
            setImage(url: url, imageView: iconImageView)
        }
        
        firstLabel.text = model.name?.first ?? ""
        secondLabel.text = model.name?.last ?? ""
    }
}
