//  
//  FriendTableManager.swift
//  Test1
//
//  Created by vaskov on 12.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import UIKit
import RxSwift

protocol FriendTableManagerDelegate: class {
    func manager(_ manager: FriendTableManager, didSelectAt indexPath: IndexPath)
    func manager(_ manager: FriendTableManager, didRemove friend: Friend)
    func manager(_ manager: FriendTableManager, model: Friend)
    func manager(_ manager: FriendTableManager, willDisplay model: Friend)
}

class FriendTableManager: BaseTableManager <FriendTableCell, Friend> {
    weak var delegate: FriendTableManagerDelegate?
    private var disposeBag = DisposeBag()
    private(set) var isEdit: Bool!

    // MARK: - Publics -
    func configManager(_ tableView: UITableView, isEdit: Bool, delegate: FriendTableManagerDelegate) {
        self.delegate = delegate
        self.isEdit = isEdit
        
        tableView.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 1, height: 0.5))
        tableView.tableFooterView?.backgroundColor = tableView.separatorColor
        super.configManager(tableView)
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.manager(self, didSelectAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isEdit
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let friend = items[indexPath.row]
        delegate?.manager(self, didRemove: friend)
        super.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }
    
    override func fillCell(_ cell: FriendTableCell, model: Friend, indexPath: IndexPath) {
        super.fillCell(cell, model: model, indexPath: indexPath)
        delegate?.manager(self, model: model)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        delegate?.manager(self, willDisplay: items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.manager(self, didSelectAt: indexPath)
    }
}
