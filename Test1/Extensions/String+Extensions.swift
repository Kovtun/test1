//
//  VKStringHelper.swift
//  VKHelpers
//
//  Created by vaskov on 25.11.16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import UIKit


extension String {
    public var localized: String {
        var bundle: Bundle!
        var text: String = ""
        let resourse = (NSLocale.preferredLanguages.first! as NSString).substring(to: 2)
        
        if ["nl", "en"].contains(resourse) {
            bundle = Bundle(path: BUNDLE.path(forResource: resourse, ofType: "lproj")!)
            text = bundle!.localizedString(forKey: self, value: nil, table: nil)
        }
        
        if text.isEmpty {
            let bundle = Bundle(path: BUNDLE.path(forResource: "en", ofType: "lproj")!)
            text = bundle!.localizedString(forKey: self, value: nil, table: nil)
        }
        return text
    }
    
    public func size(font: UIFont, width: CGFloat = CGFloat.greatestFiniteMagnitude) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        
        let boundingRect = self.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude),
                                             options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                             attributes: fontAttributes,
                                             context:nil)
        
        return boundingRect.size
    }
    
    func encodeUrl() -> String? {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    
    func decodeUrl() -> String? {
        return self.removingPercentEncoding
    }
    
    public func isEmailValid() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public func canOpenURL() -> Bool {
        let nsstring = self as NSString
        let string: String = nsstring.contains("://") ? self : "http://" + self
        let url = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        guard let url1 = URL(string: url!) else { return false }
        guard UIApplication.shared.canOpenURL(url1) else { return false }
        
        let urlPattern = "^(http|https|ftp)\\://([a-zA-Z0-9\\.\\-]+(\\:[a-zA-Z0-9\\.&amp;%\\$\\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\\-]+\\.)*[a-zA-Z0-9\\-]+\\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\\:[0-9]+)*(/($|[a-zA-Z0-9\\.\\,\\?\\'\\\\\\+&amp;%\\$#\\=~_\\-]+))*$"
        return url!.matches(pattern: urlPattern)
    }
    
    public func isPasswordValid() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^[0-9A-Za-z!\\[\\]\"#$%&'()\\^_`{|}~*+,-./:;<=>?@]{6,16}$")
        return passwordTest.evaluate(with: self)
    }
    
    public func isZipCodeValid() -> Bool {
        let zipcodePredicate = NSPredicate(format: "SELF MATCHES %@", "^\\d{5}(-\\d{4})?$")
        return zipcodePredicate.evaluate(with: self)
    }
    
    public func isNameValid() -> Bool {
        let lettersRegEx = "^([a-zA-Zа-яА-Я ]{2,30})$"
        let lettersTest = NSPredicate(format:"SELF MATCHES %@", lettersRegEx)
        return lettersTest.evaluate(with: self)
        
    }
    
    func isPhoneValid() -> Bool {
        let PHONE_REGEX = "^[0-9- \\(\\)]*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    public func date(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        return formatter.date(from: self)
    }
    
    private func matches(pattern: String) -> Bool {
        let regex = try! NSRegularExpression(
            pattern: pattern,
            options: [.caseInsensitive])
        return regex.firstMatch(
            in: self,
            options: [],
            range: NSRange(location: 0, length: utf16.count)) != nil
    }
}
