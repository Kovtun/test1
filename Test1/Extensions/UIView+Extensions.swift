//
//  UIView+Extensions.swift
//  Table
//
//  Created by vaskov on 13.07.17.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD


protocol ViewExtensionProtocol {
    var cornerRadius: CGFloat { set get }
    var borderWidth: CGFloat { set get }
    var borderColor: UIColor { set get }
    
    var left: CGFloat { set get }
    var top: CGFloat { set get }
    var right: CGFloat { set get }
    var bottom: CGFloat { set get }
    var width: CGFloat { set get }
    var height: CGFloat { set get }
    var centerX: CGFloat { set get }
    var centerY: CGFloat { set get }
    var boundWidth: CGFloat { set get }
    var boundHeight: CGFloat { set get }
    var origin: CGPoint { set get }
    var size: CGSize { set get }
    
    static func fromNib<T: UIView>() -> T
    
    func setHole(with rect: CGRect)
    func setGradient(topColor: UIColor, bottomColor: UIColor)
    func setShadow(color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool)
    func setRoundCorner(_ roundCorner: CGFloat, byRoundingCorners: UIRectCorner, width: CGFloat)
    
    func screenshot() -> UIImage
    func color(point: CGPoint) -> UIColor
    
    func showHud(animated: Bool)
    func hideHud(animated: Bool)
}

extension UIView: ViewExtensionProtocol {}


extension UIView {
    @IBInspectable public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable public var borderColor: UIColor {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)![0] as! T
    }
}

extension UIView {
    public var left: CGFloat {
        set {
            var frame = self.frame
            frame.origin.x = newValue
            self.frame = frame
        }
        get{
            return self.frame.origin.x
        }
    }
    
    public var top: CGFloat {
        set {
            var frame = self.frame
            frame.origin.y = newValue
            self.frame = frame
        }
        get{
            return self.frame.origin.y
        }
    }
    
    public var right: CGFloat {
        set {
            var frame = self.frame
            frame.origin.x = newValue - frame.size.width
            self.frame = frame
        }
        get{
            return self.frame.origin.x + self.frame.size.width;
        }
    }
    
    public var bottom: CGFloat {
        set {
            var frame = self.frame
            frame.origin.y = newValue - frame.size.height
            self.frame = frame
        }
        get{
            return self.frame.origin.y + self.frame.size.height
        }
    }
    
    public var width: CGFloat {
        set {
            var frame = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
        get{
            return self.frame.size.width
        }
    }
    
    public var height: CGFloat {
        set {
            var frame = self.frame
            frame.size.height = newValue
            self.frame = frame
        }
        get{
            return self.frame.size.height
        }
    }
    
    public var centerX: CGFloat {
        set {
            var frame = self.frame
            frame.origin.x = newValue - self.width / 2
            self.frame = frame
        }
        get{
            return self.frame.origin.x + self.width / 2
        }
    }
    
    public var centerY: CGFloat {
        set {
            var frame = self.frame
            frame.origin.y = newValue - self.height / 2
            self.frame = frame
        }
        get{
            return self.frame.origin.y + self.height / 2
        }
    }
    
    public var boundWidth: CGFloat {
        set {
            var bounds = self.bounds
            bounds.size.width = newValue
            self.bounds = bounds
        }
        get{
            return self.bounds.size.width
        }
    }
    
    public var boundHeight: CGFloat {
        set {
            var bounds = self.bounds
            bounds.size.height = newValue
            self.bounds = bounds
        }
        get{
            return self.bounds.size.height
        }
    }
    
    public var origin: CGPoint {
        set {
            var frame = self.frame
            frame.origin = newValue
            self.frame = frame
        }
        get{
            return self.frame.origin
        }
    }
    
    public var size: CGSize {
        set {
            var frame = self.frame
            frame.size = newValue
            self.frame = frame
        }
        get{
            return self.frame.size
        }
    }
}

extension UIView {
    func setHole(with rect: CGRect){
        // Create a mutable path and add a rectangle that will be h
        let mutablePath = CGMutablePath()
        mutablePath.addRect(self.bounds)
        mutablePath.addRect(rect)

        // Create a shape layer and cut out the intersection
        let mask = CAShapeLayer()
        mask.path = mutablePath
        mask.fillRule = CAShapeLayerFillRule.evenOdd

        // Add the mask to the view
        self.layer.mask = mask
    }
    
    func setGradient(topColor: UIColor, bottomColor: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        
        if self.layer.sublayers != nil {
            for layer in self.layer.sublayers! {
                if layer is CAGradientLayer {
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setShadow(color: UIColor = UIColor.black, opacity: Float = 0.25, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func setRoundCorner(_ roundCorner: CGFloat = 8, byRoundingCorners: UIRectCorner, width: CGFloat = 0) {
        var size = self.layer.bounds.size
        size.width = width != 0 ? width : size.width
        
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: byRoundingCorners,
                                cornerRadii: CGSize(width: roundCorner, height:roundCorner))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = rect
        shapeLayer.path = path.cgPath
        self.layer.mask = shapeLayer
    }
  
    public func screenshot() -> UIImage {
        let size: CGSize = self.frame.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func color(point: CGPoint) -> UIColor {
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        
        var pixelData: [UInt8] = [0, 0, 0, 0]
        
        let context = CGContext(data: &pixelData, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context?.translateBy(x: -point.x, y: -point.y)
        self.layer.render(in: context!)
        
        let red: CGFloat = CGFloat(pixelData[0])/CGFloat(255.0)
        let green: CGFloat = CGFloat(pixelData[1])/CGFloat(255.0)
        let blue: CGFloat = CGFloat(pixelData[2])/CGFloat(255.0)
        let alpha: CGFloat = CGFloat(pixelData[3])/CGFloat(255.0)
        
        let color:UIColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
}

extension UIView {
    func showHud(animated: Bool = true) {
        MBProgressHUD.showAdded(to: self, animated: animated)
    }
    
    func hideHud(animated: Bool = true) {
        MBProgressHUD.hide(for: self, animated: animated)
    }
}
