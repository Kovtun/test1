//
//  ApiBittiq.swift
//  Bittiq
//
//  Created by vaskov on 07/04/2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation


final class Link {
    #if DEBUG
        static let baseUrl = "https://randomuser.me/api/"
    #endif
    
    #if RELEASE
        static let baseUrl = "https://randomuser.me/api/"
    #endif
    
    private(set) var link: String
    private(set) var endpoint: String?
    
    init(_ endpoint: String?) {
        self.endpoint = endpoint
        link = String(format: "%@?%@", Link.baseUrl, endpoint ?? "")
    }
}

enum Endpoint: String {
    case `default` = "%@"
    case friends = ""
        
    func link(_ elements: Any...) -> Link {
        return Link(self.endpoint(elements))
    }
    
    func endpoint(_ elements: Any...) -> String? {
        guard let ar = elements.first as? [Any], ar.count != 0 else {
            return nil
        }
        let link = String(format: self.rawValue, arguments: elements as! [CVarArg])
        
        let _link = link.components(separatedBy: "=").map {
             $0.trimmingCharacters(in: CharacterSet(charactersIn: "() \n"))
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }.joined(separator: "=") as NSString
        
        return _link.substring(to: 1) == "/" ? _link.substring(from: 1) : _link as String
    }
}

