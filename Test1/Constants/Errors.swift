//
//  Errors.swift
//  Test1
//
//  Created by vaskov on 17.08.18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import Alamofire


enum Test1Error: Error, Equatable {
    case `default`
    case coreDataError
    
    static func showAlert(with error: Error?, title: String = "ERROR") {
        if let text = Test1Error.text(with: error), !text.isEmpty {
            AlertHelper.showMessage(message: AlertMessage(title: title, message: text))
        } 
    }
    
    static func text(with error: Error?) -> String? {
        if let error = error, let text = error is Test1Error ? (error as! Test1Error).text() : error.localizedDescription {
            return text.isEmpty ? nil : text
        }
        return nil
    }
    
    func text() -> String? {
        let text = textOfAlert()
        return text.isEmpty ? nil : text
    }
}

// MARK: - Private -
extension Test1Error {
    fileprivate func textOfAlert() -> String {
        switch self {
        case .default:
            return "ERROR_SERVER_ERROR"
        case .coreDataError:
            return "Core Data Error"
        }
    }
}

// MARK: - Equatable -
func ==(lhs: Error, rhs: Error) -> Bool {
    return lhs._code == rhs._code && lhs._domain == rhs._domain
}

func !=(lhs: Error, rhs: Error) -> Bool {
    return !(lhs == rhs)
}

func ==(lhs: Test1Error, rhs: Test1Error) -> Bool {
    switch (lhs, rhs) {
    case (.`default`, .`default`):
        return true
    case (.coreDataError, .coreDataError):
        return true
    default:
        return false
    }
}

func !=(lhs: Test1Error, rhs: Test1Error) -> Bool {
    return !(lhs == rhs)
}

func ==(lhs: Test1Error, rhs: Error) -> Bool {
    return false
}

func ==(lhs: Error, rhs: Test1Error) -> Bool {
    return false
}

func !=(lhs: Test1Error, rhs: Error) -> Bool {
    return true
}

func !=(lhs: Error, rhs: Test1Error) -> Bool {
    return true
}

