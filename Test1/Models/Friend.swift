//  
//  Friend.swift
//  Test1
//
//  Created by vaskov on 12.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import RxCocoa

class Friend: BaseModel, PaginatedResponseProtocol {
    var id: String?
    var name: Name?
    var picture: Picture?
    var email: String?
    var phone: String?
    
    private var id1: Dictionary<String, Any>?
    
    // MARK:- BaseModel -
    override func mapping(map: Map) {
        id1 <- map["id"]
        id = id1?["value"] as? String
        name <- map["name"]
        picture <- map["picture"]
        email <- map["email"]
        phone <- map["phone"]
    }
    
    class func friends() -> PaginatedResponse<FriendRequest, Friend> {
        return  PaginatedResponse.init(Endpoint.friends.link(), request: FriendRequest())
    }
}

