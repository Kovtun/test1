//
//  Name.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import ObjectMapper


class Name: BaseModel {
    var title: String?
    var first: String?
    var last: String?
    
    override func mapping(map: Map) {
        title <- map["title"]
        first <- map["first"]
        last <- map["last"]
    }
}


