//
//  DBPicture+CoreDataProperties.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//
//

import Foundation
import CoreData


extension DBPicture {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBPicture> {
        return NSFetchRequest<DBPicture>(entityName: "DBPicture")
    }

    @NSManaged public var medium: String?
    @NSManaged public var thumbnail: String?
    @NSManaged public var large: String?
    @NSManaged public var friend: DBFriend?

}

