//
//  DBUser+CoreDataProperties.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//
//

import Foundation
import CoreData


extension DBUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBUser> {
        return NSFetchRequest<DBUser>(entityName: "DBUser")
    }

    @NSManaged public var isFriend: Bool
    @NSManaged public var friend: DBFriend?

}
