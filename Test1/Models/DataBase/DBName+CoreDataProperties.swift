//
//  DBName+CoreDataProperties.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//
//

import Foundation
import CoreData


extension DBName {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBName> {
        return NSFetchRequest<DBName>(entityName: "DBName")
    }

    @NSManaged public var title: String?
    @NSManaged public var last: String?
    @NSManaged public var first: String?
    @NSManaged public var friend: DBFriend?

}
