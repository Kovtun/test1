//
//  DB+Extenisons.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import CoreData


extension DBUser {
    class func new(with friend: Friend, context: NSManagedObjectContext) -> DBUser {
        let dbUser: DBUser = CORE_DATA_MANAGER.new(with: context)!
        dbUser.isFriend = true
        dbUser.friend = DBFriend.new(with: friend, context: context)
        return dbUser
    }
}


extension DBFriend {
    class func new(with friend: Friend, context: NSManagedObjectContext) -> DBFriend {
        let dbFriend: DBFriend = CORE_DATA_MANAGER.new(with: context)!
        dbFriend.id = friend.id
        dbFriend.email = friend.email
        dbFriend.phone = friend.phone
        dbFriend.picture = DBPicture.new(with: friend.picture ?? Picture(), context: context)
        dbFriend.name = DBName.new(with: friend.name ?? Name(), context: context)
        return dbFriend
    }
    
    func friend() -> Friend {
        let friend = Friend()
        friend.id = self.id
        friend.email = self.email
        friend.phone = self.phone
        friend.picture = self.picture?.picture()
        friend.name = self.name?.name()
        
        return friend
    }
}


extension DBPicture {
    class func new(with picture: Picture, context: NSManagedObjectContext) -> DBPicture {
        let dbPicture: DBPicture = CORE_DATA_MANAGER.new(with: context)!
        dbPicture.large = picture.large
        dbPicture.thumbnail = picture.thumbnail
        dbPicture.medium = picture.medium
        
        return dbPicture
    }
    
    func picture() -> Picture {
        let picture = Picture()
        picture.large = self.large
        picture.thumbnail = self.thumbnail
        picture.medium = self.medium
        
        return picture
    }
}


extension DBName {
    class func new(with name: Name, context: NSManagedObjectContext) -> DBName {
        let dbName: DBName = CORE_DATA_MANAGER.new(with: context)!
        dbName.title = name.title
        dbName.first = name.first
        dbName.last = name.last
        
        return dbName
    }
    
    func name() -> Name {
        let name = Name()
        name.title = self.title
        name.first = self.first
        name.last = self.last
        
        return name
    }
}
