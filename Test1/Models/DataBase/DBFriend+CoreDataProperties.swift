//
//  DBFriend+CoreDataProperties.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//
//

import Foundation
import CoreData


extension DBFriend {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBFriend> {
        return NSFetchRequest<DBFriend>(entityName: "DBFriend")
    }

    @NSManaged public var id: String?
    @NSManaged public var email: String?
    @NSManaged public var phone: String?
    @NSManaged public var picture: DBPicture?
    @NSManaged public var name: DBName?
    @NSManaged public var user: DBUser?

}
