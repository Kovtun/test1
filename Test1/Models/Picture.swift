//
//  Picture.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import ObjectMapper


class Picture: BaseModel {
    var large: String?
    var thumbnail: String?
    var medium: String?
    
    override func mapping(map: Map) {
        large <- map["large"]
        thumbnail <- map["thumbnail"]
        medium <- map["medium"]
    }
}

