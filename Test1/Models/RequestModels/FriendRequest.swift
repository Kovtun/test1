//
//  FriendRequest.swift
//  Test1
//
//  Created by vaskov on 13.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import ObjectMapper


class FriendRequest: BaseModel, PaginatedRequesProtocol {
    var page: Int = 0
    var seed: String?
    var results: Int = 10
    
    override func mapping(map: Map) {
        page <- map["page"]
        seed <- map["seed"]
        results <- map["results"]
    }
    
    init(page: Int = 0, seed: String? = nil, results: Int = 10) {
        self.page = page
        self.seed = seed
        self.results = results
        super.init()
    }
    
    required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
}
