//
//  JXBaseModel.swift
//  Cosmunity
//
//  Created by vaskov on 12.09.18.
//  Copyright © 2018 Home Inc. All rights reserved.
//

import ObjectMapper
import AlamofireObjectMapper


class BaseModel: NSObject, Mappable {
    func mapping(map: Map) {
    }

    override init() {
        super.init()
    }
    
    required init?(map: Map) {
    }
}


