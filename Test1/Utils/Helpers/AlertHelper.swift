//
//  AlertHelper.swift
//
//  Created by vaskov on 19.04.18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit


struct AlertHelper {
    static var staticController: UIViewController?
    
    static func showAlertController(title: String?, message: String?, controller: UIViewController?, okButtonTitle okTitle: String, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        DispatchQueue.main.async {
            staticController = controller
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: okTitle, style: UIAlertAction.Style.default, handler: { action in
                if handler != nil { handler!(action) }
                staticController = nil
            } ))
            controller?.present(alert, animated: true, completion: nil)
        }
    }
    
    static func showMessage<T: AlertMessageProtocol>(message: T, controller: UIViewController? = UIViewController.topViewController(), okButtonTitle okTitle: String = "ok") {
        showAlertController(title: message.title, message: message.message, controller: controller, okButtonTitle: okTitle, handler: nil)
    }
    
    static func showMessage<T: AlertMessageProtocol>(message: T, controller: UIViewController?, okButtonTitle okTitle: String = "OK", handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        showAlertController(title: message.title, message: message.message, controller: controller, okButtonTitle: okTitle, handler: handler)
    }
    
    static func showMessage<T: AlertMessageProtocol>(message: T, fromController controller: UIViewController?, okButtonTitle okTitle: String, cancelButtonTitle cancelTitle: String, okHandler: ((UIAlertAction) -> Swift.Void)? = nil, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        DispatchQueue.main.async {   
            staticController = controller
            let alert = UIAlertController(title: message.title, message: message.message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: okTitle, style: .default, handler: { action in
                if okHandler != nil { okHandler!(action) }
                staticController = nil
            } ))
            alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler:  { action in
                if cancelHandler != nil { cancelHandler!(action) }
                staticController = nil
            }))
            controller?.present(alert, animated: true, completion: nil)
        }
    }
}
