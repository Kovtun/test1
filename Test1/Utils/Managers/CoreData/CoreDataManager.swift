//
//  CoreDataManager.swift
//  Astronavigation
//
//  Created by vaskov on 4/17/19.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation
import CoreData
import RxCocoa
import RxSwift

typealias ContextHanler = ((_ context: NSManagedObjectContext) -> Void)?

protocol CoreDataManagerProtocol {
    var mainContext: NSManagedObjectContext {get}
    var backgroundContext: NSManagedObjectContext {get}
    
    func copyDBToDocuments() -> Observable<Bool>
    func clear() -> Observable<Bool>
    
    func new<T: NSManagedObject>(with context: NSManagedObjectContext) -> T?
    func new<T: NSManagedObject>(contextType: CoreDataManager.ContextType, responseType: T.Type) -> (T?, NSManagedObjectContext)
    
    func newAndSave<T: NSManagedObject>(with context: NSManagedObjectContext,
                                        responseType: T.Type) -> Observable<T?>
    func newAndSave<T: NSManagedObject>(contextType: CoreDataManager.ContextType,
                                        responseType: T.Type) -> Observable<(T?, NSManagedObjectContext)>
    
    func save(with context: NSManagedObjectContext) -> Observable<NSManagedObjectContext> // the same contexts but there may be different threads
    func save(contextType: CoreDataManager.ContextType) -> Observable<NSManagedObjectContext>
    
    func read<T: NSManagedObject>(with context: NSManagedObjectContext,
                                  predicate: NSPredicate?,
                                  responseType: T.Type) -> Observable<[T]>
    func read<T: NSManagedObject>(contextType: CoreDataManager.ContextType,
                                  predicate: NSPredicate?,
                                  responseType: T.Type) -> Observable<([T], NSManagedObjectContext)>
    
    func update<T: NSManagedObject>(with context: NSManagedObjectContext,
                                    predicate: NSPredicate?,
                                    responseType: T.Type) -> Observable<[T]>
    func update<T: NSManagedObject>(contextType: CoreDataManager.ContextType,
                                    predicate: NSPredicate?,
                                    responseType: T.Type) -> Observable<([T], NSManagedObjectContext)>
    
    func delete<T: NSManagedObject>(with context: NSManagedObjectContext,
                                    predicate: NSPredicate?,
                                    responseType: T.Type) -> Observable<Void>
    func delete<T: NSManagedObject>(contextType: CoreDataManager.ContextType,
                                    predicate: NSPredicate?,
                                    responseType: T.Type) -> Observable<Void>
    
    func delete(contextType: CoreDataManager.ContextType, object: NSManagedObject) -> Observable<Void>
    func delete(with context: NSManagedObjectContext, object: NSManagedObject) -> Observable<Void>
}


class CoreDataManager: CoreDataManagerProtocol {
    enum ContextType {
        case main
        case background
    }
    static let shared = CoreDataManager()
    private var disposeBag = DisposeBag()
    
    var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    var backgroundContext: NSManagedObjectContext {
        return persistentContainer.newBackgroundContext()
    }
    
    private init() {
        copyDBToDocuments().map({ (_) in
            _ = self.persistentContainer
        }).subscribe().disposed(by: disposeBag)
    }
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        let storeDescription = NSPersistentStoreDescription(url: dbURL())
        storeDescription.isReadOnly = false
        storeDescription.setOption(NSNumber(booleanLiteral: true), forKey: NSIgnorePersistentStoreVersioningOption)
        container.persistentStoreDescriptions = [storeDescription]

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error \(error)")
            } else {
                print("Path to DB")
                print(container.persistentStoreDescriptions.first as Any)
            }
        })
        return container
    }()
    
    func copyDBToDocuments() -> Observable<Bool> {
        return Observable.create { (observer) -> Disposable in
            let bundleURL = URL(fileURLWithPath: BUNDLE.path(forResource: "DB", ofType: "sqlite")!)

            if !FILE_MANAGER.fileExists(atPath: self.dbPath()) {
                do {
                    try FILE_MANAGER.copyItem(at: bundleURL, to: self.dbURL())
                    observer.onNext(true)
                } catch {
                    observer.onError(error)
                }
            } else {
                observer.onNext(false)
            }

            observer.onCompleted()
            return Disposables.create()
        }
    }

    func clear() -> Observable<Bool> {
        return Observable.create { (observer) -> Disposable in
            if FILE_MANAGER.fileExists(atPath: self.dbPath())  {
                do {
                    try FILE_MANAGER.removeItem(atPath: self.dbPath())
                    observer.onNext(true)
                } catch {
                    observer.onError(error)
                }
            } else {
                observer.onNext(false)
            }
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func new<T: NSManagedObject>(with context: NSManagedObjectContext) -> T? {
        guard let entity = NSEntityDescription.entity(forEntityName: String(describing: T.self), in: context) else {
            return nil
        }
        return T(entity: entity, insertInto: context)
    }

    func new<T: NSManagedObject>(contextType: CoreDataManager.ContextType = .main,
                                 responseType: T.Type) -> (T?, NSManagedObjectContext) {
        let context = contextType == .main ? mainContext : backgroundContext
        return (new(with: context), context)
    }

    func newAndSave<T: NSManagedObject>(with context: NSManagedObjectContext,
                                        responseType: T.Type) -> Observable<T?> {
        let object: T? = new(with: context)
        return save(with: context).map { _ in
            return object
        }
    }

    func newAndSave<T: NSManagedObject>(contextType: CoreDataManager.ContextType = .main,
                                        responseType: T.Type) -> Observable<(T?, NSManagedObjectContext)>  {
        let arg = new(contextType: contextType, responseType: T.self)
        return save(with: arg.1).map { context -> (T?, NSManagedObjectContext) in
            return (arg.0, context)
        }
    }
    
    func save(with context: NSManagedObjectContext) -> Observable<NSManagedObjectContext> {
        return Observable.create { observer in
           context.perform ({
               do {
                    observer.onNext((context))
                    observer.onCompleted()
                    try context.save()
               } catch {
                    observer.onError(error)
               }
           })
           return Disposables.create()
       }
    }

    func save(contextType: CoreDataManager.ContextType = .main) -> Observable<NSManagedObjectContext> {
        let context = contextType == .main ? self.mainContext : self.backgroundContext
        context.automaticallyMergesChangesFromParent = contextType == .background

        return save(with: context)
    }

    func read<T: NSManagedObject>(with context: NSManagedObjectContext,
                                  predicate: NSPredicate?,
                                  responseType: T.Type) -> Observable<[T]> {
        return Observable.create { (observer) -> Disposable in
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
            request.predicate = predicate
            do {
                if let result = try context.fetch(request) as? [T] {
                    observer.onNext(result)
                } else {
                    observer.onNext([])
                }
                observer.onCompleted()
            } catch {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func read<T: NSManagedObject>(contextType: CoreDataManager.ContextType = .main,
                                  predicate: NSPredicate? = nil,
                                  responseType: T.Type) -> Observable<([T], NSManagedObjectContext)> {
        let context = contextType == .main ? self.mainContext : self.backgroundContext
        return read(with: context, predicate: predicate, responseType: T.self).map { (items) -> ([T], NSManagedObjectContext) in
            return (items, context)
        }
    }

    func update<T: NSManagedObject>(with context: NSManagedObjectContext,
                                    predicate: NSPredicate?,
                                    responseType: T.Type) -> Observable<[T]> {
        return read(with: context, predicate: predicate, responseType: T.self).flatMap ({ (items) -> Observable<[T]> in
            return self.save(with: context).map { (_) -> [T] in
                return items
            }
        })
    }

    func update<T: NSManagedObject>(contextType: CoreDataManager.ContextType = .main,
                                    predicate: NSPredicate? = nil,
                                    responseType: T.Type) -> Observable<([T], NSManagedObjectContext)> {
        return read(contextType: contextType, predicate: predicate, responseType: T.self).flatMap ({ (arg) -> Observable<([T], NSManagedObjectContext)> in
            return self.save(with: arg.1).map { (context) -> ([T], NSManagedObjectContext) in
                return (arg.0, context)
            }
        })
    }
    
    func delete<T: NSManagedObject>(with context: NSManagedObjectContext, predicate: NSPredicate?, responseType: T.Type) -> Observable<Void> {
        return update(with: context, predicate: predicate, responseType: T.self).map { items in
            for item in items {
                context.delete(item)
            }
        }
    }
    
    func delete<T: NSManagedObject>(contextType: CoreDataManager.ContextType, predicate: NSPredicate?, responseType: T.Type) -> Observable<Void> {
        return update(contextType: contextType, predicate: predicate, responseType: T.self).map { (items, context) in
            for item in items {
                context.delete(item)
            }
        }
    }
    
    func delete(with context: NSManagedObjectContext, object: NSManagedObject) -> Observable<Void> {
        return save(with: context).map { (context1) in
            context1.delete(object)
        }
    }
    
    func delete(contextType: CoreDataManager.ContextType, object: NSManagedObject) -> Observable<Void> {
        return save(contextType: contextType).map { (context1) in
            context1.delete(object)
        }
    }
}

// MARK: - Privates -
extension CoreDataManager {
    private func dbPath() -> String {
        var documentsUrl = getDocumentsDirectory()
        documentsUrl.appendPathComponent("DB.sqlite")
        
        var urlStr = documentsUrl.absoluteString
        urlStr.removeFirst("file://".count)
        
        return urlStr
    }
    
    private func dbURL() -> URL {
        var dbURL = getDocumentsDirectory()
        dbURL.appendPathComponent("DB.sqlite")

        return dbURL
    }
   
   private func getDocumentsDirectory() -> URL {
       let paths = FILE_MANAGER.urls(for: .documentDirectory, in: .userDomainMask)
       return paths[0]
   }
}
