//
//  PaginatedResponse.swift
//  Cosmunity
//
//  Created by vaskov on 3/14/17.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import RxSwift
import RxCocoa


protocol PaginatedResponseDelegate: class {
    func loadBegan(_ response: AnyObject)
    func loadFinished(_ response: AnyObject)
    func loadErrored(_ response: AnyObject, error: NSError)
}

protocol AnonymousPaginatedResponse: class {
    func startReload()
    func startNextPage()
}

protocol PaginatedRequesProtocol: Mappable {
    var page: Int {set get}
    var seed: String? {set get}
    var results: Int {set get}
}

protocol PaginatedResponseProtocol: Mappable {
    var id: String? {set get}
}


class Info: BaseModel {
    var version: String?
    var results: Int?
    var seed: String?
    var page: Int?

    override func mapping(map: Map) {
        version <- map["version"]
        results <- map["results"]
        seed <- map["seed"]
        page <- map["page"]
    }
}


class PaginatedResponse<Request: PaginatedRequesProtocol, Response: PaginatedResponseProtocol>: BaseModel, AnonymousPaginatedResponse {
    var info: Info!
    var items: [Response] = []
    var hasMoreItems: Bool = true
    
    weak var delegate: PaginatedResponseDelegate?
    private(set) var isLoading: Bool = false
    private(set) var link: Link?
    
    private let bag = DisposeBag()
    private var defaultRequest: Request!
    private var request: Request!
    private var currentTask: Observable<PaginatedResponse<Request, Response>>?

    //MARK: - Initialization -
    init(_ link: Link, request: Request) {
        self.link = link
        self.defaultRequest = request
        self.request = request
        
        super.init()
        reset()
    }

    required init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }

    //MARK: - BaseModel -
    override func mapping(map: Map) {
        info <- map["info"]
        items <- map["results"]
    }

    //MARK: - Publics -
    func cancel() {
        self.currentTask = nil
    }

    func reset() {
        self.currentTask = nil
        self.items = []
        self.request = self.defaultRequest
        self.hasMoreItems = true
    }

    func reload() -> Observable<PaginatedResponse<Request, Response>> {
        reset()
        return task(isNext: false)
    }

    func nextPage() -> Observable<PaginatedResponse<Request, Response>> {
        request.page += 1
        return task(isNext: true)
    }

    // MARK: AnonymousPaginatedResponse
    func startReload() {
        reload().subscribe().disposed(by: bag)
    }

    func startNextPage() {
        nextPage().subscribe().disposed(by: bag)
    }
}

// MARK: - Privates -
extension PaginatedResponse {
    private func task(isNext: Bool) -> Observable<PaginatedResponse<Request, Response>> {
        guard currentTask == nil else {
            return currentTask!
        }

        currentTask = guardTask(isNext: isNext)
        self.delegate?.loadBegan(self)
        self.isLoading = true

        return self.currentTask!
    }

    private func guardTask(isNext: Bool) -> Observable<PaginatedResponse<Request, Response>> {
        return ApiService.get(link!, body: request as AnyObject, responseType: (PaginatedResponse<Request, Response>).self).map { [weak self] (response) -> PaginatedResponse<Request, Response> in
            self?.isLoading = false            
            self?.hasMoreItems = response.items.count == self?.request.results
            self?.request.seed = response.info.seed
            
            if isNext {
                self?.items.insert(contentsOf: response.items, at: (self?.items.count ?? 1) - 1)
                if let hasMoreItems = self?.hasMoreItems, !hasMoreItems {
                    self?.items.remove(at: (self?.items.count ?? 1) - 1)
                }
            } else {
                self?.items = response.items
                if let hasMoreItems = self?.hasMoreItems, hasMoreItems {
                    var item = Response.init(map: Map(mappingType: MappingType.fromJSON, JSON: ["":""]))!
                    item.id = Constants.emptyId
                    self?.items.append(item)
                }
            }

            if self != nil {
                self!.delegate?.loadFinished(self!)
            }
            
            self?.currentTask = nil
            return response
        }.catchError { [weak self] (error) -> Observable<PaginatedResponse<Request, Response>> in
            if self != nil {
                self!.delegate?.loadErrored(self!, error: error as NSError)
            }
            self?.isLoading = false
            self?.currentTask = nil
            self?.hasMoreItems = false
            return Observable.error(error)
        }
    }
}
