//
//  CashImagesProtocol.swift
//  CrowdThinc
//
//  Created by vaskov on 05.02.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import PINRemoteImage

protocol CashImagesProtocol {
    func image(url: String?, completion: @escaping (UIImage?) -> ())
    func setImage(url: String?, placeholder: UIImage?, imageView: UIImageView?)
    func setImage(url: String?, placeholder: UIImage?, button: UIButton?)
}

extension CashImagesProtocol {
    func image(url: String?, completion: @escaping (UIImage?) -> ())  {
        guard url != nil else {
            completion(nil)
            return
        }
        let imageView = UIImageView()
        imageView.setImage(url: url) {
            completion(imageView.image)
        }
    }
    
    func setImage(url: String?, placeholder: UIImage? = nil, imageView: UIImageView?) {
        guard let imageView = imageView else {
            return
        }
        
        if let url = url, !isThereImageInCach(url) {
            //imageView.showHud(animated: false)
        }
        
        imageView.setImage(url: url, placeholder: placeholder) {
            //imageView.hideHud(animated: false)
        }
    }
    
    func setImage(url: String?, placeholder: UIImage? = nil, button: UIButton?) {
        guard let button = button else {
            return
        }
        
        if let url = url, !isThereImageInCach(url) {
            button.showHud(animated: false)
        }
        
        button.setImage(url: url, placeholder: placeholder) {
            button.hideHud(animated: false)
        }
    }
}

// MARK: - CashImagesProtocol -
extension CashImagesProtocol {
    fileprivate func isThereImageInCach(_ url: String?) -> Bool {
        guard let url = url else {
            return false
        }
        func cacheFileUrl(_ fileName: String) -> URL {
            let cacheURL = FILE_MANAGER.urls(for: .cachesDirectory, in: .userDomainMask).first!
            return cacheURL.appendingPathComponent(fileName)
        }
        
        let key = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? url
        let fileURL = cacheFileUrl(key)
        return FILE_MANAGER.fileExists(atPath: fileURL.absoluteString)
    }
}

// MARK: - UIImageView -
extension UIImageView {
    fileprivate func setImage(url: String?, placeholder: UIImage? = nil, completion: @escaping () -> ()) {
        if let urlString = url {
            self.pin_setImage(from: URL(string: urlString)) { (result) in
                completion()
            }
        } else {
            self.image = placeholder
        }
    }
}

// MARK: - UIButton -
extension UIButton {
    fileprivate func setImage(url: String?, placeholder: UIImage? = nil, completion: @escaping () -> ()) {
        if let urlString = url {
            self.pin_setImage(from: URL(string: urlString)) { (result) in
                completion()
            }
        } else {
            self.setImage(placeholder, for: .normal)
        }
    }
}
