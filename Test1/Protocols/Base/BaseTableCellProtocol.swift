//
//  BaseTableCellProtocol.swift
//  Helpers
//
//  Created by vaskov on 09.09.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit


protocol BaseTableCellProtocol: class {
    associatedtype Model
    static var identifier: String {get}
    
    static func height(_ model: Model) -> CGFloat // has already implemented
    func identifier() -> String                   // has already implemented
    func update(_ model: Model)
}

extension BaseTableCellProtocol {
    func identifier() -> String {
        return type(of: self).identifier
    }
    
    static func height(_ model: Model) -> CGFloat {
        return UITableView.automaticDimension
    }
}



