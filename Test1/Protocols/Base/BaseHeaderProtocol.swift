//
//  BaseHeaderProtocol.swift
//  Test1
//
//  Created by vaskov on 22/11/2018.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit


protocol BaseHeaderProtocol {
    associatedtype HeaderModel
    static var identifier: String {get}
    
    static func height(_ model: HeaderModel) -> CGFloat
    func update(_ model: HeaderModel)
}
